describe("React appliccation home page", () => {
  it("verify that the applinks says learn react", () => {
    browser.url("/");
    let text = $(".App-link").getText();
    assert.equal(text, "Learn React");
  });
});
